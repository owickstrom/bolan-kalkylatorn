# Bolån-kalkylatorn

Räkna ut engångs- och månadskostnader för ditt bolån

## Instruktioner

Installera beroenden:

```bash
$ npm install
````

Starta utvecklingsserver:

```bash
$ npm run start
```

Eller, bygg komplett webbsida:

```bash
$ npm run build
```

## Licens

[Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/)

Baserad på [Parcel TypeScript React Example](https://github.com/parcel-bundler/examples/tree/master/typescript-react)
